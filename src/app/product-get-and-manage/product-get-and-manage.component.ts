import { ProductAddComponent } from './../product-add/product-add.component';
import { ProductGetComponent } from './../product-get/product-get.component';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Subject } from 'rxjs';
import Product from '../Product';


@Component({
  selector: 'app-product-get-and-manage',
  templateUrl: './product-get-and-manage.component.html',
  styleUrls: ['./product-get-and-manage.component.css']
})
export class ProductGetAndManageComponent implements OnInit {

  public parentRefreshNeeded: Subject<boolean> = new Subject();

  productCreated(product) {
    console.log('ProductGetAndManageComponent: productCreated(${product})');
    this.parentRefreshNeeded.next(true);
  }

  productDeleted(product) {
    console.log('ProductGetAndManageComponent: productDeleted(${product})');
    this.parentRefreshNeeded.next(true);
  }

  constructor() { }

  ngOnInit() {



  }

}
