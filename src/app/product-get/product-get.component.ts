// product-get.component.ts

import { Component, OnInit, Output, EventEmitter, Input, SimpleChange } from '@angular/core';
import Product from '../Product';
import { ProductsService } from '../products.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-product-get',
  templateUrl: './product-get.component.html',
  styleUrls: ['./product-get.component.css']
})
export class ProductGetComponent implements OnInit {

  products: Product[];
  constructor(private ps: ProductsService) { }

  ngOnInit() {

    this.refreshData();
    this.ps.dataChanged.subscribe(v => {
      console.log('ProductGetComponent: refreshData()', v);
      this.refreshData();
   });

  }

  public refreshData() {
    this.ps
      .getProducts()
      .subscribe((data: Product[]) => {
        this.products = data;
    });

    console.log('ProductGetComponent: refreshData');

  }

  deleteProduct(id) {

    // This shows the deleted item
    // this.ps.deleteProduct(id).subscribe(res => {
    //   this.products.splice(id, 1);
    // });

    // this.ps.deleteProduct(id)
    // .subscribe((data: Product[]) => {
    //   this.products = data;
    // });

    this.ps.deleteProduct(id);
    this.refreshData();

  }

}

