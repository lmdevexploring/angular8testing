// products.service.ts

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  uri = 'http://localhost:4000/products';
  public dataChanged = new Subject<boolean>();

  constructor(private http: HttpClient) { }

  addProduct(ProductName, ProductDescription, ProductPrice) {
    console.log(ProductName, ProductDescription, ProductPrice);
    const obj = {
      ProductName,
      ProductDescription,
      ProductPrice
    };

    this.http.post(`${this.uri}/add`, obj)
        .subscribe(res => {
          this.dataChanged.next(true);
          console.log('Done');
        });
  }

  getProducts() {
    return this.http.get(`${this.uri}`);
  }

  editProduct(id) {
    return this.http.get(`${this.uri}/edit/${id}`);
    }

  updateProduct(ProductName, ProductDescription, ProductPrice, id) {
    const obj = {
      ProductName,
      ProductDescription,
      ProductPrice
    };
    this
      .http
      .post(`${this.uri}/update/${id}`, obj)
      .subscribe(res => {
        this.dataChanged.next(true);
        console.log('Done');
      });
  }

  deleteProduct(id) {
      return this
              .http
              .get(`${this.uri}/delete/${id}`)
              .subscribe(res => {
                this.dataChanged.next(true);
                console.log('Done');
              });
  }

}

