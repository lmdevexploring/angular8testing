**Angular 8 Testing**

Angular 8 + Typescript + MongoDB

## HTTP Server
ng serve -o

---

## node.js api service
nodemon server

---

## Inspiration

CRUD Example
https://appdividend.com/2019/06/04/angular-8-tutorial-with-example-learn-angular-8-crud-from-scratch/

Observable and Subscriptions
https://www.djamware.com/post/5da31946ae418d042e1aef1d/angular-8-tutorial-observable-and-rxjs-examples

Event Emitter
https://medium.com/@pandukamuditha/angular-5-share-data-between-sibling-components-using-eventemitter-8ebb49b64a0a

Debugging

Chrome Extension: https://chrome.google.com/webstore/detail/augury/elgalmkoelokbchhkhacckoklkejnhcd?hl=en


